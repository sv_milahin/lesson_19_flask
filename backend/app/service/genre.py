from app.dao.genre import GenreDAO


class GenreService:
    def __init__(self, dao):
        self.dao = dao

    def get_all(self):
        return self.dao.get_all()

    def get_one(self, pk):
        return self.dao.get_one(pk)

    def create(self, data):
        return self.dao.create(data)

    def update(self, data):
        pk = data.get('id') 
        genre = self.get_one(pk)
        genre.name = data.get('name')
      
        self.dao.update(genre)
    
    def delete(self, pk):
        self.dao.delete(pk)
