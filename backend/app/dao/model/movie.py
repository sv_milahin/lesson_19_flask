from marshmallow_sqlalchemy import SQLAlchemySchema, auto_field

from app.database import db
from app.dao.model.director import Director
from app.dao.model.genre import Genre


class Movie(db.Model):
    __tablename__ = 'movies'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(521), nullable=False)
    description = db.Column(db.String(1024), nullable=False)
    trailer = db.Column(db.String(128), nullable=False)
    year = db.Column(db.Integer, nullable=False)
    rating = db.Column(db.String(5), nullable=False)
    genre_id = db.Column(db.Integer, db.ForeignKey(Genre.id))
    director_id = db.Column(db.Integer, db.ForeignKey(Director.id))
    genre = db.relationship(Genre)
    director = db.relationship(Director)

    def __repr__(self):
        return f'<Director({self.name=})>'


class MovieSchema(SQLAlchemySchema):
    class Meta:
        model = Movie
        include_relationships = True
        load_instance = True
    
    id = auto_field()
    title = auto_field()
    description = auto_field()
    trailer = auto_field()
    year = auto_field()
    rating = auto_field()
    genre_id = auto_field()
    director_id = auto_field()

movie_schema = MovieSchema()
movies_schema = MovieSchema(many=True)

