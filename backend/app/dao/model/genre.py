from marshmallow_sqlalchemy import SQLAlchemySchema,auto_field

from app.database import db


class Genre(db.Model):
    __tablename__ = 'genres'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(32), nullable=False)

    def __repr__(self):
        return f'<Genre({self.name=})'


class GenreSchema(SQLAlchemySchema):
    class Meta:
        model = Genre
        load_instance = True
    
    id = auto_field()
    name = auto_field()

genre_schema = GenreSchema()
genres_schema = GenreSchema(many=True)
