from app.dao.model.movie import Movie


class MovieDAO:

    def __init__(self, session):
        self.session = session

    def get_all(self):
        return self.session.query(Movie).all()

    def get_one(self, pk):
        return self.session.query(Movie).get_or_404(pk)

    def create(self, data):
        movie = Movie(**data)
        
        self.session.add(movie)
        self.session.commit()

    def update(self, data):
        self.session.add(data)
        self.session.commit()

    def delete(self, pk):
        self.session.delete(self.get_one(pk))
        self.session.commit()

    def get_genre(self, pk):
        return self.session.query(Movie).filter(Movie.genre_id == pk)

    def get_director(self, pk):
        return self.session.query(Movie).filter(Movie.director_id == pk)

    def get_year(self, year):
        return self.session.query(Movie).filter(Movie.year == year)



