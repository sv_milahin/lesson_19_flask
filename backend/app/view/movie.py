from flask import abort, request
from flask_restx import Resource, Namespace
from flask_jwt_extended import jwt_required, get_jwt

from app.dao.model.movie import movie_schema, movies_schema
from app.container import movie_service


movie_ns = Namespace('movies')


@movie_ns.route('/')
class MoviesView(Resource):
    @jwt_required()
    def get(self):
        genre = request.args.get('genre_id')
        director = request.args.get('director_id')
        year = request.args.get('year')

        if genre:
            movie = movie_service.get_genre(genre)
        elif director:
            movie = movie_service.get_director(director)
        elif year:
            movie = movie_service.get_year(year)
        else:
            movie = movie_service.get_all()

        return movies_schema.dump(movie), 200
    @jwt_required()
    def post(self):
        req = request.get_json()
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            movie = movie_service.create(req)
            return '', 201, {'location': f'/movies/{movie}'}
        abort(401)


@movie_ns.route('/<int:pk>')
class MovieView(Resource):
    @jwt_required()
    def get(self, pk):
        movie = movie_service.get_one(pk)
        return movie_schema.dump(movie), 200
    @jwt_required()
    def put(self, pk):
        req = request.get_json()
        req['id'] = pk
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            movie_service.update(req)
            return '', 200
        abort(401)

    @jwt_required()
    def delete(self, pk):
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            movie_service.delete(pk)
            return '', 204 
        abort(401)

