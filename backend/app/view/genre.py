from flask import abort, request
from flask_restx import Resource, Namespace
from flask_jwt_extended import jwt_required, get_jwt

from app.dao.model.genre import genre_schema, genres_schema
from app.container import genre_service


genre_ns = Namespace('genres')


@genre_ns.route('/')
class GenresView(Resource):
    @jwt_required()
    def get(self):
        genre = genre_service.get_all()
        return genres_schema.dump(genre), 200
    @jwt_required()
    def post(self):
        req = request.get_json()
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            genre = genre_service.create(req)
            return '', 201, {'location': f'/genres/{genre}'}
        abort(401)

@genre_ns.route('/<int:pk>')
class GenreView(Resource):
    @jwt_required()
    def get(self, pk):
        genre = genre_service.get_one(pk)
        return genre_schema.dump(genre), 200
    @jwt_required()
    def put(self, pk):
        req = request.get_json()
        req['id'] = pk
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            genre_service.update(req)
            return '', 200
        abort(401)

    @jwt_required()
    def delete(self, pk):
        role_admin = get_jwt()
        if role_admin['role'] == 'admin':
            genre_service.delete(pk)
            return '', 204 
        abort(401)

