from flask import request, abort
from flask_jwt_extended import (
        create_access_token,
        create_refresh_token,
        get_jwt_identity,
        jwt_required)

from flask_restx import Resource, Namespace
from werkzeug.security import check_password_hash

from app.dao.model.user import User
from app.container import user_service

auth_ns = Namespace('auth')

@auth_ns.route('/signup')
class SingupView(Resource):

    def post(self):
        req = request.get_json()
        user = user_service.create(req)
        return '', 201

@auth_ns.route('/login')
class LoginView(Resource):
    
    def post(self):
        req = request.get_json()

        email = req.get('email')
        password = req.get('password')

        user = user_service.get_email(email)
    
        if (user is not None) and check_password_hash(user.password, password):
            access_token = create_access_token(identity=user.name,
                    additional_claims={'role': user.role})
            refresh_token = create_refresh_token(identity=user.name,
                    additional_claims={'role': user.role})

            res = {
                    'access_token': access_token,
                    'refresh_token': refresh_token,
                    }
            return res, 200
        return abort(401)



@auth_ns.route('/refresh')
class RefreshView(Resource):
    @jwt_required(refresh=True)
    def post(self):
        name = get_jwt_identity()

        access_token = create_access_token(identity=name)
        return {'access_token': access_token}, 200
        
